module.exports = {
  name: 'future-shell',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/libs/future-shell',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js',
  ],
};
