import { ActionReducerMap, MetaReducer } from '@ngrx/store';
import { AppState } from './app.interface';
import { routerReducer } from '@ngrx/router-store';
import { storeFreeze } from 'ngrx-store-freeze';
import { environment } from '../../../../shared/src/lib/environments/environment';
import { taskReducer } from '../../../../../apps/todo-app/src/app/modules/dashboard/state/reducers/tasks.reducer';

const STORE_KEYS_TO_PERSIST = ['timezone', 'notifications'];

export const appReducer: ActionReducerMap<AppState> = {
  router: routerReducer,
  tasks: taskReducer
}

export const appMetaReducers: MetaReducer<AppState>[] = !environment.production
  ? [storeFreeze]
  : [];
