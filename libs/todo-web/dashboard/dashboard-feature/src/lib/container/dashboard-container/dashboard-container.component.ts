import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';


import { TaskModel } from '@test/shared';
import {
  getDoneTasks,
  getTodoTasks
} from '@test/todo-web/dashboard/data-access';
import { AddTask, LoadTasks, UpdateTask } from '@test/todo-web/dashboard/data-access';
import { AppState } from '@test/todo-web/dashboard/data-access';

@Component({
  selector: 'test-dashboard-container',
  templateUrl: './dashboard-container.component.html',
  styleUrls: ['./dashboard-container.component.scss']
})
export class DashboardContainerComponent implements OnInit {
  todoTasks: TaskModel[];
  doneTasks: TaskModel[];

  constructor(private store: Store<AppState>) {
    store.pipe(select(getTodoTasks)).subscribe(res => {
      this.todoTasks = res;
    });

    store.pipe(select(getDoneTasks)).subscribe(res => {
      this.doneTasks = res;
    });
  }

  ngOnInit(): void {
    this.store.dispatch(LoadTasks());
  }

  addTask(event): void {
    this.store.dispatch(AddTask(event));
  }

  changeStatus(event: TaskModel): void {
    this.store.dispatch(UpdateTask({
      id: event.id,
      changes: { name: event.name, status: event.status }
    }));
  }
}
