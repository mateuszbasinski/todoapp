import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardContainerComponent } from './container/dashboard-container/dashboard-container.component';
import { DashboardComponent } from './presenter/dashboard/dashboard.component';
import { DashboardListComponent } from './presenter/dashboard-list/dashboard-list.component';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatTabsModule } from '@angular/material/tabs';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TodoWebDashboardFeatureRoutingModule } from './todo-web-dashboard-feature-routing.modle';
import { DashboardListMobileComponent } from './presenter/dashboard-list-mobile/dashboard-list-mobile.component';

@NgModule({
  declarations: [
    DashboardContainerComponent,
    DashboardComponent,
    DashboardListComponent,
    DashboardListMobileComponent
  ],
  imports: [
    CommonModule,
    TodoWebDashboardFeatureRoutingModule,
    MatInputModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTabsModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule
  ]
})
export class TodoWebDashboardDashboardFeatureModule {
}
