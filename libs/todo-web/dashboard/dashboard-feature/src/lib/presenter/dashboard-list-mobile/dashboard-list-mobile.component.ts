import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TaskModel } from '@test/shared';

@Component({
  selector: 'test-dashboard-list-mobile',
  templateUrl: './dashboard-list-mobile.component.html',
  styleUrls: ['./dashboard-list-mobile.component.scss']
})
export class DashboardListMobileComponent implements OnInit {
  @Input() todoTasks: TaskModel[];
  @Input() doneTasks: TaskModel[];
  @Output() clickEmit = new EventEmitter();
  checked = true;

  constructor() { }

  ngOnInit(): void {
  }

  taskClick(task: TaskModel): void {
    task = {
      id: task.id,
      name: task.name,
      status: !task.status
    };

    this.clickEmit.emit(task);
  }

}
