import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardListMobileComponent } from './dashboard-list-mobile.component';

describe('DashboardListMobileComponent', () => {
  let component: DashboardListMobileComponent;
  let fixture: ComponentFixture<DashboardListMobileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardListMobileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardListMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
