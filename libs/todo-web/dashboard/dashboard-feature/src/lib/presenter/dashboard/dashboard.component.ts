import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { AppState, selectIds } from '@test/todo-web/dashboard/data-access';

@Component({
  selector: 'test-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  @Output() taskEmit = new EventEmitter();
  name = new FormControl();

  constructor(private store: Store<AppState>) {
  }

  ngOnInit(): void {
  }

  addTaskEmit(): void {
    let id;

    this.store.pipe(select(selectIds)).subscribe(res => {
      id = Number(res[res.length - 1]) + 1;
    });

    this.taskEmit.emit({ id: id, name: this.name.value, status: false });
  }
}
