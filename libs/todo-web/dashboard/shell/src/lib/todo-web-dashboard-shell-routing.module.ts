import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('../../../dashboard-feature/src/lib/todo-web-dashboard-dashboard-feature.module').then(m => m.TodoWebDashboardDashboardFeatureModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class TodoWebDashboardShellRoutingModuleModule {
}
