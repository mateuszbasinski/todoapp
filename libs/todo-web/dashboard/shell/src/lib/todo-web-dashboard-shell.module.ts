import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TodoWebDashboardDashboardFeatureModule } from '@test/todo-web/dashboard/dashboard-feature';
import { TodoWebDashboardShellRoutingModuleModule } from './todo-web-dashboard-shell-routing.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    TodoWebDashboardDashboardFeatureModule,
    TodoWebDashboardShellRoutingModuleModule
  ]
})
export class TodoWebDashboardShellModule {
}
