module.exports = {
  name: 'todo-web-dashboard-data-access',
  preset: '../../../../jest.config.js',
  coverageDirectory: '../../../../coverage/libs/todo-web/dashboard/data-access',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js',
  ],
};
