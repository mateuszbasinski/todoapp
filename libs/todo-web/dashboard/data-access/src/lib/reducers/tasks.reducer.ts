import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { createFeatureSelector, createReducer, on } from '@ngrx/store';
import * as taskActions from '../actions/tasks.action';
import { TaskModel, Tasks } from '@test/shared';

export interface TasksState extends EntityState<TaskModel> {
  error: string;
}

export const taskAdapter: EntityAdapter<TaskModel> = createEntityAdapter<TaskModel>();

export const initialTaskState: TasksState = taskAdapter.getInitialState({
  error: ''
});

export const taskReducer = createReducer(initialTaskState,
  on(taskActions.LoadTasks, (state) => {
    return taskAdapter.addMany(Tasks, {
      ...state,
      error: ''
    });
  }), on(taskActions.AddTask, (state, action) => {
    return taskAdapter.addOne(action, state);
  }), on(taskActions.UpdateTask, (state, action) => {
    return taskAdapter.updateOne(action, state);
  })
);

export const selectTaskState = createFeatureSelector<TasksState>('tasks');

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal
} = taskAdapter.getSelectors(selectTaskState);
