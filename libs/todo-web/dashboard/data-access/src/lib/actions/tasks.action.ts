import { createAction, props } from '@ngrx/store';
import { TaskModel } from '@test/shared';

export const LoadTasks = createAction(
  '[Dashboard] load tasks'
);

export const LoadTasksSuccess = createAction(
  '[Dashboard] load tasks',
  props<{ tasks: TaskModel[] }>()
);

export const AddTask = createAction(
  '[Dashboard] Add task',
  props<{ id: number, name: string, status: boolean }>()
);

export const UpdateTask = createAction(
  '[Dashboard] Update task',
  props<{ id: number, changes: { name: string, status: boolean } }>()
);
