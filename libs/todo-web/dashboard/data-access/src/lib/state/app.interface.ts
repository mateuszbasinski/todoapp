import { RouterReducerState } from '@ngrx/router-store';
import { RouterStateUrl } from './state-utiles';
import { TasksState } from '../reducers/tasks.reducer';

export interface AppState {
  router: RouterReducerState<RouterStateUrl>;
  tasks: TasksState
}
