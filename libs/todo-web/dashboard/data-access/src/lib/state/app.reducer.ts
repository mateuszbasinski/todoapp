import { ActionReducerMap, MetaReducer } from '@ngrx/store';
import { AppState } from './app.interface';
import { routerReducer } from '@ngrx/router-store';
import { storeFreeze } from 'ngrx-store-freeze';
import { taskReducer } from './../reducers/tasks.reducer';
import { environment } from '@test/shared';

const STORE_KEYS_TO_PERSIST = ['timezone', 'notifications'];

export const appReducer: ActionReducerMap<AppState> = {
  router: routerReducer,
  tasks: taskReducer
}

export const appMetaReducers: MetaReducer<AppState>[] = !environment.production
  ? [storeFreeze]
  : [];
