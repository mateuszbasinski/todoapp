import { createSelector } from '@ngrx/store';
import { selectAll } from './reducers/tasks.reducer';
import { TaskModel } from '@test/shared';

export const getTodoTasks = createSelector(
  selectAll,
  (teacher: TaskModel[]) => {
    return teacher.filter(x => x.status === false);
  }
)

export const getDoneTasks = createSelector(
  selectAll,
  (teacher: TaskModel[]) => {
    return teacher.filter(x => x.status === true);
  }
)
