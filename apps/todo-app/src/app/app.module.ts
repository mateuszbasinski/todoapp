import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '@test/shared';
import { StateModule } from '@test/todo-web/dashboard/data-access';
import { TodoWebDashboardShellRoutingModuleModule } from '@test/todo-web/dashboard/shell';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule,
    TodoWebDashboardShellRoutingModuleModule,
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production
    }),
    StateModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
